<?php

use Parser\Parser;
use Parser\RossvyazExcerptParser;

require_once 'vendor/autoload.php';

$parser = new Parser;
$parser->setParser(new RossvyazExcerptParser)
    ->setFilePath($argv[1])
    ->parse()
    ->save();
