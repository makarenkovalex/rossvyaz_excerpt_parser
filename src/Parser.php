<?php

namespace Parser;

use Error;

class Parser
{
    protected $parser;

    public function setParser($parser): Parser
    {
        $this->parser = $parser;
        return $this;
    }

    public function setFilePath(string $filePath): Parser
    {
        $this->parser->setFilePath($filePath);
        return $this;
    }

    public function parse(): Parser
    {
        if (isset($this->parser)) {
            $this->parser->parse();
            return $this;
        } else {
            throw new Error("Please set concrete parser or filePath before processing");
        }
    }

    public function save(): Parser
    {
        $this->parser->save();
        return $this;
    }
}