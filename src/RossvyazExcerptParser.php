<?php

namespace Parser;

use SplFileObject;
use Error;

class RossvyazExcerptParser
{
    const POSITION_PREFIX = 0;
    const POSITION_OPERATOR = 4;

    protected $filePath;

    protected $operators;

    public function setFilePath(string $filePath): RossvyazExcerptParser
    {
        $this->filePath = $filePath;
        return $this;
    }

    public function parse(): RossvyazExcerptParser
    {
        $file = new SplFileObject($this->filePath);
        while (!$file->eof()) {
            $this->processLine($file->current());
            $file->next();
        }
        return $this;
    }

    public function save(): RossvyazExcerptParser
    {
        $fileName = dirname(__DIR__, 1).'/output/'.date("Y_m_d_His").'.csv';
        $fp = fopen($fileName, 'x+');
        foreach ($this->operators as $operator => $prefixes) {
            foreach ($prefixes as $prefix => $true) {
                fwrite($fp, $operator.';'.$prefix.PHP_EOL);
            }
        }
        return $this;
    }

    protected function processLine(string $line): void
    {
        $lineArray = explode(';', $line);
        if (
            isset($lineArray[self::POSITION_PREFIX]) && 
            isset($lineArray[self::POSITION_OPERATOR])
        ) {
            $operator = $lineArray[self::POSITION_OPERATOR];
            $prefix = $lineArray[self::POSITION_PREFIX];
            $this->operators[$operator][$prefix] = true;
        }
    }
}